package com.twu.biblioteca;

public class BookNotValidException extends Exception {
    BookNotValidException(String message) {
        super(message);
    }
}
