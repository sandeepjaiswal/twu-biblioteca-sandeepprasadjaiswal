package com.twu.biblioteca;

import java.io.PrintStream;
import java.util.Scanner;

public class ConsoleIO implements IoDriver{

    private PrintStream out;
    private Scanner scanner;


    public ConsoleIO(PrintStream out, Scanner scanner) {
        this.out = out;
        this.scanner = scanner;
    }


    public void print(String message) {
        out.println(message);
    }

    public String read() {
        return scanner.nextLine();
    }
}
