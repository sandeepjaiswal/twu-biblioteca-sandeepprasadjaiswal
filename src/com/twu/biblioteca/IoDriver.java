package com.twu.biblioteca;

public interface IoDriver {
    void print(String message);
    String read();
}
