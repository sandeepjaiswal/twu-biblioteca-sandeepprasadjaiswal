package com.twu.biblioteca;

//Represents a recording of moving images
public class Movie {
    private final String name;
    private final String year;
    private final String director;
    private final Integer rating;

    public Movie(String name, String year, String director, Integer rating) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
    }


    public String name() {
        return this.name;
    }
}
