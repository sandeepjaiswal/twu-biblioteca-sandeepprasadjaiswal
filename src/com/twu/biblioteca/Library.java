package com.twu.biblioteca;

import java.util.*;

// Collection of books
class Library {
    private HashMap<String, Book> availableBooks;
    private HashMap<String, Book> checkedoutBooks;
    private List<Movie> availableMovies;
    private List<Movie> checkedoutMovies;


    Library(HashMap<String, Book> availableBooks, List<Movie> availableMovies) {
        this.availableBooks = availableBooks;
        checkedoutBooks = new HashMap<String, Book>();
        this.availableMovies=availableMovies;
        checkedoutMovies = new ArrayList<Movie>();
    }

    void displayAvailableBooks() {
//        System.out.println("No.\t" + " Title\t\t\t" + " Author \t\t\t"+" Year Published");
        for (Map.Entry<String, Book> entry : availableBooks.entrySet()) {
            System.out.println(entry.getKey() + "\t"+entry.getValue());
        }
    }

    void displayCheckedoutBooks(){
        System.out.println("No. " + " Title" + " Author "+" Year Published");
        for (Map.Entry<String, Book> entry : checkedoutBooks.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    void displayAvailableMovies() {
//        System.out.println("No.\t" + " Title\t\t\t" + " Author \t\t\t"+" Year Published");
        for(Movie movie:availableMovies){
            System.out.print(movie);
        }
    }


    void checkoutBook(String bookNumber) throws BookNotAvailableException {
        if (!availableBooks.containsKey(bookNumber)) {
            throw new BookNotAvailableException("Sorry, that book is not available");
        } else {
            checkedoutBooks.put(String.valueOf(checkedoutBooks.size()+1), availableBooks.get((bookNumber)));
            availableBooks.remove(bookNumber);
            System.out.println("Thank You Enjoy the Book");
        }
    }

    void returnBook(String bookNumber) throws BookNotValidException {
        if (!checkedoutBooks.containsKey((bookNumber))) {
            throw new BookNotValidException("Not a valid book");
        } else {
            availableBooks.put(String.valueOf(availableBooks.size()+1),checkedoutBooks.get((bookNumber)));
            checkedoutBooks.remove(bookNumber);
            System.out.print("Thank you for returning the book");
        }
    }



    HashMap<String, Book> availableBooks() {
        return new HashMap<String, Book>(availableBooks);
    }

    void checkoutMovie(String movieName) throws MovieNotAvailableException {
        if(findMovieIndex(movieName)==-1){
            throw new MovieNotAvailableException();
        }
        else {
             checkedoutMovies.add(availableMovies.get(findMovieIndex(movieName)));
             availableMovies.remove(findMovieIndex(movieName));
        }
    }


    private int findMovieIndex(String movieName){
        for(int index=0;index<availableMovies.size();index++){
            if(availableMovies.get(index).name().equalsIgnoreCase(movieName)){
                return index;
            }
        }
        return -1;
    }

    public List<Movie> availableMovies() {
        return availableMovies;
    }
}
