package com.twu.biblioteca;

import java.util.*;

public class Application {
    public static void main(String[] args) throws BookNotAvailableException {
        HashMap<String, Book> availableBooks =
                new HashMap<String, Book>() {{
                    put("1", new Book("Head First Java", "Baites", "1995"));
                    put("2", new Book("To Kill a mockingbird", "Harper lee", "1990"));
                }};
        List<Movie> availableMovies = new ArrayList<Movie>(Arrays.asList(
                new Movie("Boyhood","2015","Richard Linkater",9),
                new Movie("Avengers Infinity War","2018","Russo Brothers",8)
        ));
        Library library = new Library(availableBooks,availableMovies);
        ConsoleIO consoleIO = new ConsoleIO(System.out, new Scanner(System.in));
        Biblioteca biblioteca = new Biblioteca(library, consoleIO);
        biblioteca.displayWelcomeMessage();
        biblioteca.displayMainMenu();

    }
}
