package com.twu.biblioteca;

public class BookNotAvailableException extends Exception {
    BookNotAvailableException(String message){
        super(message);
    }
}
