package com.twu.biblioteca;

//Manages a library
class Biblioteca {

    private Library library;
    private ConsoleIO consoleIO;

    Biblioteca(Library library, ConsoleIO consoleIO) {
        this.library = library;
        this.consoleIO = consoleIO;
    }

    void displayWelcomeMessage() {
        consoleIO.print("Welcome to Library." +
                " Your one-stop-shop for great book titles in Bangalore");
    }

    void displayMainMenu() {
        while (true) {
            System.out.println("Menu\n1.List of Books\n2.Checkout Books\n3.Return Book\n4.Quit");

            String userInput = consoleIO.read();
            if (userInput.equals("1")) {
                library.displayAvailableBooks();
            } else if (userInput.equals("2")) {
                consoleIO.print("Enter Book Number to checkout");
                library.displayAvailableBooks();
                String bookNumber = consoleIO.read();
                try {
                    library.checkoutBook(bookNumber);
                } catch (BookNotAvailableException bookNotAvailableException) {
                    consoleIO.print("Book Not Available");
                }
            } else if (userInput.equals("3")) {
                library.displayCheckedoutBooks();
                consoleIO.print("Enter Book Number to Return");
                String bookNumber = consoleIO.read();
                try {
                    library.returnBook(bookNumber);
                } catch (BookNotValidException e) {
                    consoleIO.print("That is not a valid book to return.");
                }
            } else if (userInput.equals("4")) {
                break;
            } else {
                consoleIO.print("Please select a valid option");
            }
        }


    }

}
