package com.twu.biblioteca;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.PrintStream;
import java.util.Scanner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ConsoleIOTest {

    @Test
    void expectsToPrintMessage() {
        PrintStream printStream = mock(PrintStream.class);
        Scanner scanner = new Scanner(System.in);

        ConsoleIO consoleIO = new ConsoleIO(printStream, scanner);

        consoleIO.print("Hello");

        verify(printStream).println("Hello");
    }

    @Disabled
    @Test
    void expectsToAcceptInput() {
        PrintStream printStream = mock(PrintStream.class);
        Scanner scanner = mock(Scanner.class);

        ConsoleIO consoleIO = new ConsoleIO(printStream , scanner);

        consoleIO.read();

        verify(scanner).nextLine();
    }
}