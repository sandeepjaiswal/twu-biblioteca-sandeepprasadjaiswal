package com.twu.biblioteca;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class LibraryTest {

    HashMap<String,Book> availableBooks =
            new HashMap<String, Book>(){{
                put("1",new Book("Head First Java","Baites","1995"));
                put("2",new Book("To Kill a mockingbird","Harper lee","1990"))  ;
            }};
    List<Movie> availableMovies = new ArrayList<Movie>(Arrays.asList(
       new Movie("Boyhood","2015","Richard Linkater",9),
       new Movie("Avengers Infinity War","2018","Russo Brothers",8)
    ));



    @Test
    public void expectsABookToBeRemovedAfterCheckout() throws BookNotAvailableException {
        Library library = new Library(availableBooks,availableMovies);

        library.checkoutBook("1");
        HashMap<String, Book> listOfAvailableBooks = library.availableBooks();

        assertEquals(false,listOfAvailableBooks.containsKey("1"));
    }

    @Disabled
    @Test
    public void expectsMessageOnUnsuccessfullCheckout() throws BookNotAvailableException {
        HashMap<String,Book> availableBooks =
                new HashMap<String, Book>(){{
                    put("1",new Book("Head First Java","Baites","1995"));
                    put("2",new Book("To Kill a mockingbird","Harper lee","1990"))  ;
                }};

        Library library = new Library(availableBooks,availableMovies);
        library.checkoutBook("5");
    }

    @Test
    public void expectsBookToBeReturned() throws BookNotValidException, BookNotAvailableException {
        Library library = new Library(availableBooks,availableMovies);

        library.checkoutBook("1");
        library.returnBook("1");

        HashMap<String, Book> listOfAvailableBooks = library.availableBooks();

        assertEquals(true,listOfAvailableBooks.containsKey("1"));
    }

    @Test
    public void expectsAMovieToBeRemovedAfterCheckout() throws BookNotAvailableException, MovieNotAvailableException {
        Library library = new Library(availableBooks,availableMovies);

        library.checkoutMovie("Boyhood");
        List<Movie> listOfAvailableMovies = library.availableMovies();

        assertEquals(false,listOfAvailableMovies.contains("Boyhood"));
    }


}