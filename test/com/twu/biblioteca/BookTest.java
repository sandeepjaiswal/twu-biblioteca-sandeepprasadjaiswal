package com.twu.biblioteca;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookTest {

    @Test
    public void expectsBookToBeCreatedWithNameHeadFirstJava(){
        Book book = new Book("Head First java","Baites","1995");
        assertEquals("Head First java,Baites,1995",book.toString());
    }
}
