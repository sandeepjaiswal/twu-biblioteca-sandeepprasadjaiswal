package com.twu.biblioteca;


import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class BibliotecaTest {

    @Test
    void expectsWelcomeMessageToBeDisplayed() {
        Library library = mock(Library.class);

        ConsoleIO out = mock(ConsoleIO.class);
        Biblioteca bangaloreLibrary = new Biblioteca(library, out);

        bangaloreLibrary.displayWelcomeMessage();

        verify(out).print("Welcome to Library. " +
                "Your one-stop-shop for great book titles in Bangalore");

    }

    @Test
    void expectsLibraryToDisplayBookWhenUserInputIs1() {
        Library library = mock(Library.class);
        ConsoleIO consoleIO = mock(ConsoleIO.class);
        Biblioteca bangaloreLibrary = new Biblioteca(library, consoleIO);

        when(consoleIO.read()).thenReturn("1").thenReturn("4");
        bangaloreLibrary.displayMainMenu();

        verify(library).displayAvailableBooks();
    }

    @Test
    void expectsLibraryToCheckoutBookWhenUserInputIs2() throws BookNotAvailableException {
        Library library = mock(Library.class);
        ConsoleIO consoleIO = mock(ConsoleIO.class);
        Biblioteca bangaloreLibrary = new Biblioteca(library, consoleIO);

        when(consoleIO.read()).thenReturn("2").thenReturn("1").thenReturn("4");
        bangaloreLibrary.displayMainMenu();

        verify(library).checkoutBook("1");

    }

    @Test
    void expectsErrorMessageWhenUserInputIsIncorrect(){
        Library library = mock(Library.class);
        ConsoleIO consoleIO = mock(ConsoleIO.class);
        Biblioteca bangaloreLibrary = new Biblioteca(library, consoleIO);

        when(consoleIO.read()).thenReturn("9").thenReturn("4");
        bangaloreLibrary.displayMainMenu();

        verify(consoleIO).print("Please select a valid option");
    }




}